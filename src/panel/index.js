const enableLogging = true;

/* MFP Settings */
const loginUrlMyFitnessPal = 'https://www.myfitnesspal.com/account/login';
const logoutUrlMyFitnessPal = 'https://www.myfitnesspal.com/account/logout';
const tokenEndpointMyFitnessPal = 'https://www.myfitnesspal.com/user/auth_token?refresh=true';
const diaryEndpointStubMyFitnessPal = 'https://api.myfitnesspal.com/v2/diary?fields%5B%5D=all&types=food_entry%2Cexercise_entry%2Csteps_aggregate&entry_date='; // date=2018-11-09

/* Cronometer settings */
const loginUrlCronometer = 'https://mobile.cronometer.com/api/v2/login';
const logoutUrlCronometer = 'https://mobile.cronometer.com/api/v2/logout';
const diaryEndpointCronometer = 'https://mobile.cronometer.com/api/v2/get_diary';

var loginButton = document.getElementById('login-button');
loginButton.addEventListener("click", loginClicked);

var logoutButton = document.getElementById('clear-button');
logoutButton.addEventListener("click", logoutClicked);

var updateButton = document.getElementById('submit-button');
updateButton.addEventListener("click", updateClicked);

var serviceDropdown = document.getElementById('service');
serviceDropdown.addEventListener("change", serviceSelectionUpdated);

var savePassword = document.getElementById('save-password');
savePassword.addEventListener("change", savePasswordUpdated);

var usernameInput = document.getElementById('username');
usernameInput.addEventListener("change", updateStoredUsernameAndPassword);

var passwordInput = document.getElementById('username');
passwordInput.addEventListener("change", updateStoredUsernameAndPassword);

start();

function start(){
	browser.storage.local.get('userInfo').then(
		function(data){
			if (data && data.userInfo){
				var userInfo = data.userInfo;
				log('userInfo' + JSON.stringify(userInfo));
	
				// we retrieved some data from storage
				if (userInfo.isLoggedIn){
					// we're already logged in
					showUpdateButton();
					showLogoutButton();
				}
				else {
					// set predefined values, if any
					log('userInfo.username: ' + userInfo.username);
					if (userInfo.username){
						log('setting username to: ' + username);
						document.getElementById('username').value = userInfo.username;
					}
	
					if (userInfo.savePassword){
						log('userInfo.savePassword is TRUE');
						document.getElementById('save-password').checked = userInfo.savePassword;
	
						if (userInfo.password){
							log('setting password to: ' + userInfo.password);
							document.getElementById('password').value = userInfo.password;
						}
					}
					else {
						log('userInfo.savePassword is FALSE');
					}
					
					var serviceDropdown = document.getElementById('service');
					serviceDropdown.value = userInfo.serviceName;
		
					showLoginForm();
				}			
			}
			else {
				// user hasn't logged in before
				// show the login form
				document.getElementById('login-form').classList.remove('hidden');		
			}
		},
		function(error){
			log('error');
		}
	)
}

function logoutClicked(){
	log('logoutClicked()');

	browser.storage.local.get('userInfo').then(
		function(data){
			var userInfo = data.userInfo;
			log('userInfo: ' + JSON.stringify(userInfo));

			userInfo.isLoggedIn = false;

			if (userInfo.savePassword){
				log('userInfo.password is true, keeping password');
			} else {
				log('userInfo.password is false, clearing password');
				userInfo.password = "";
				document.getElementById('password').value = "";
			}

			browser.storage.local.set({userInfo});

			setStatusText('logged out');

			hideStatusTextAfterSeconds(1.2);
		
			hideLogoutButton();

			hideUpdateButton();
			clearBadgeText();
		
			start();			
	});
}

function clearBadgeText(){
	log('clearBadgeText()');
	browser.browserAction.setBadgeText({text: ''});
}

function updateClicked(){
	log('updateClicked()');

	browser.storage.local.get('userInfo').then(function(data){
		log('data: ' + JSON.stringify(data));
		var userInfo = data.userInfo;
		var serviceName = userInfo.serviceName;
		log('serviceName: ' + serviceName);

		if (serviceName === "MyFitnessPal"){
			log('triggering MFP update');
			triggerUpdateMyFitnessPal();
		} else if (serviceName === "Cronometer"){
			log('triggering Cronometer update');
			triggerUpdateCronometer();
		} else {
			// whoops!!
		}
	});	
}

function serviceSelectionUpdated(){
	log('serviceSelectionUpdated()');

	var serviceDropdown = document.getElementById('service');
	var serviceName = serviceDropdown.options[serviceDropdown.selectedIndex].value;

	log('selected service name: ' + serviceName);

	browser.storage.local.get('userInfo').then(
		function(data){
			if (data && data.userInfo){
				log('userInfo exists, updating serviceName');
				var userInfo = data.userInfo;
				userInfo.serviceName = serviceName;
			}
			else {
				log('userInfo does not exist, creating');
				var userInfo = {
					serviceName: serviceName
				};
				log('userInfo: ' + userInfo);
			}
			browser.storage.local.set({userInfo});		
		},
		function(error){
			log('error');
		}
	)	
}

function savePasswordUpdated(){
	log('savePasswordUpdated()');

	var savePassword = document.getElementById('save-password').checked;

	log('save password: ' + savePassword);

	browser.storage.local.get('userInfo').then(
		function(data){
			if (data && data.userInfo){
				log('userInfo exists, updating savePassword');
				var userInfo = data.userInfo;
				userInfo.savePassword = savePassword;

				userInfo.password = document.getElementById('password').value;
			}
			else {
				log('userInfo does not exist, creating');
				var userInfo = {
					savePassword: savePassword
				};
				log('userInfo: ' + userInfo);
			}
			browser.storage.local.set({userInfo});		
		},
		function(error){
			log('error');
		}
	)	
}

function updateStoredUsernameAndPassword(){
	log('updateStoredUsernameAndPassword()');

	var savePassword = document.getElementById('save-password').checked
	var username = document.getElementById('username').value;

	log('username: ' + username);

	browser.storage.local.get('userInfo').then(
		function(data){
			if (data && data.userInfo){

				log('userInfo exists, updating username');
				var userInfo = data.userInfo;
				userInfo.username = username;
				log('username: ' + userInfo.username);

				if (savePassword){
					log('save password is checked, updating password');

					var password = document.getElementById('password').value;
			
					log('password: ' + password);
					userInfo.password = password;					
				}

				browser.storage.local.set({userInfo});	
			}
			else {
				log('userInfo does not exist, creating');
				var userInfo = {
					username: username
				};

				if (savePassword){
					log('save password is checked, updating password');

					var password = document.getElementById('password').value;
			
					log('password: ' + password);
					userInfo.password = password;					
				}

				log('userInfo: ' + userInfo);


				browser.storage.local.set({userInfo});		
			}
		},
		function(error){
			log('error');
		}
	)	
}

function triggerUpdateMyFitnessPal(){
	log('triggerUpdateMyFitnessPal()');

	// set the background text to U for update
	browser.browserAction.setBadgeText({text: 'U'});

	browser.storage.local.get('token').then(function(data){
		var token = data.token;

		getDiaryForTodayMyFitnessPal(token);
	});
}

function triggerUpdateCronometer(){
	log('triggerUpdateCronometer()');

	// set the background text to U for update
	browser.browserAction.setBadgeText({text: 'U'});

	browser.storage.local.get('sessionKey').then(function(data){
		var sessionKey = data.sessionKey;

		browser.storage.local.get('userInfo').then(function(data){
			var userInfo = data.userInfo;
			log('userInfo: ' + userInfo);

			getDiaryForTodayCronometer(sessionKey, userInfo.userId);
		});
	});
}

function loginClicked(){
	log('loginClicked()');

	var serviceDropdown = document.getElementById('service');
	var serviceName = serviceDropdown.options[serviceDropdown.selectedIndex].value;

	var username = document.getElementById('username').value;
	var password = document.getElementById('password').value;
	var savePassword = document.getElementById('save-password').checked;

	var userInfo = {
		'username': username,
		'serviceName': serviceName,
		'savePassword': savePassword
	};

	log('login clicked, userInfo: ' + JSON.stringify(userInfo));
	saveUserInfo(userInfo);

	userInfo.password = password;

	if (userInfo.savePassword){
		log('login clicked, savePassword, userInfo: ' + JSON.stringify(userInfo));
		saveUserInfo(userInfo);
	}

	if (serviceName === "MyFitnessPal"){
		loginMyFitnessPal(username, password, userInfo);
	} else if (serviceName === "Cronometer"){
		loginCronometer(username, password, userInfo);
	} else {
		//something went wrong
	}
}

function loginMyFitnessPal(username, password, userInfo){
	log('loginMyFitnessPal()');
	
	// first we need to load the login page, and parse 1 thing:
	// 1) the authenticity_token, from the html ('name' == 'authenticity_token')
	// NOTE: we DON'T need to parse the cookie or deal with that - the browser does it for us

	// load the login page
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4) {
			log(xhttp);

			var parser = new DOMParser();
			var doc = parser.parseFromString(xhttp.responseText, "text/html");

			var authenticity_token = doc.getElementsByName('authenticity_token')[0].value;
			browser.storage.local.set({authenticity_token});

			realLoginMyFitnessPal(userInfo, authenticity_token);
		}
	};
	xhttp.open("GET", loginUrlMyFitnessPal, true);
	xhttp.send();

	setStatusText('Logging in...');
	showStatusText();
}

function loginCronometer(username, password, userInfo){
	log('loginCronometer()');

/*
{
   "timezone": "America\/New_York",
   "mobile_login": "v2",
   "password": "passwordpassword",
   "email": "cronometer2@imultibox.com",
   "auth": {
      "build": 268,
      "os": "Android",
      "api": 2,
      "userId": 0
   }
}
*/

	var jsonData = {
		"timezone": "America\/New_York",
		"mobile_login": "v2",
		"password": password,
		"email": username,
		"auth": {
		   "build": 268,
		   "os": "Android",
		   "api": 2,
		   "userId": 0
		}
	}
	log("jsonData: " + JSON.stringify(jsonData));

	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4) {
			hideStatusText();
			log(xhttp);
			if (xhttp.status == 200){

				// if we're NOT logged in, the response text will contain {"result":"FAIL","error":"Invalid email or password"}
				if (xhttp.responseText.includes('Invalid email or password')){
					log('not logged in');
					setStatusText('Invalid email or password');
					showStatusText();
				} else {
					// we're logged in, hooray!
					log('LOGGED IN YAY');
					var response = JSON.parse(xhttp.responseText);

					var sessionKey = response.sessionKey;
					log('sessionKey: ' + sessionKey);				
					browser.storage.local.set({sessionKey});

					var userId = response.id;
					log('userId: ' + userId);
					userInfo.userId = userId;
			
					browser.storage.local.set({userInfo});


					setStatusText('Logged in');
					showStatusText();
					hideStatusTextAfterSeconds(5);

					setUserAsLoggedIn();
					showUpdateButton();
					showLogoutButton();

					hideLoginForm();

					triggerUpdateCronometer();
				}

			}
		}
	};
	xhttp.open("POST", loginUrlCronometer, true);
	xhttp.setRequestHeader('Content-type', 'application/json; charset=utf-8');
	xhttp.setRequestHeader('User-Agent', 'okhttp/3.10.0');
	xhttp.send(JSON.stringify(jsonData));
}

function realLoginMyFitnessPal(userInfo, authenticity_token){
	log('realLoginMyFitnessPal()');

	var formData = "utf8=%E2%9C%93&authenticity_token=" + authenticity_token + "&username=" + userInfo.username + "&password=" + userInfo.password + '&remember_me=1';
	log("formData: " + formData);

	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4) {
			hideStatusText();
			log(xhttp);
			if (xhttp.status == 200){

				// if we're NOT logged in, the response text will contain 'Incorrect username or password'
				if (xhttp.responseText.includes('Incorrect username or password')){
					log('not logged in');
					setStatusText('Incorrect username or password');
					showStatusText();
				} else {
					// we're logged in, hooray!
					log('LOGGED IN YAY');
					browser.storage.local.set({authenticity_token});

					setStatusText('Logged in');
					showStatusText();
					hideStatusTextAfterSeconds(5);

					setUserAsLoggedIn();
					showUpdateButton();
					showLogoutButton();

					hideLoginForm();

					getTokenFromServerMyFitnessPal(userInfo, xhttp.responseText);
				}

			}
		}
	};
	xhttp.open("POST", loginUrlMyFitnessPal, true);
	xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	xhttp.send(formData);
}

function getTokenFromServerMyFitnessPal(userInfo, html){
	log('getTokenFromServerMyFitnessPal()');

	setStatusText('Retrieving user token...');
	showStatusText();

	// call the token endpoint
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4) {
			log(xhttp);

			var token = JSON.parse(xhttp.response);
			var access_token = token.access_token;
			var expires_in = token.expires_in;
			var refresh_token = token.refresh_token;
			var user_id = token.user_id;

			browser.storage.local.set({token});

			log(token);
			log(access_token);
			log(expires_in);
			log(refresh_token);
			log(user_id);

			setStatusText('Beginning API query...');
			hideStatusTextAfterSeconds(2);
			
			triggerUpdateMyFitnessPal();
		}
	};
	xhttp.open("GET", tokenEndpointMyFitnessPal, true);
	xhttp.send();
}

function getDiaryForTodayMyFitnessPal(token){
	log('getDiaryForTodayMyFitnessPal()');
	setStatusText('Updating...');

	// load the diary today
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4) {
			log(xhttp);

			var response = JSON.parse(xhttp.responseText);
			log(response);

			hideStatusText();
			processDiaryResponseMyFitnessPal(response);
		}
	};

	// diaryEndpointStub is lacking the date part, that should be tacked on to the end. ex: 2018-11-09
	var myDate = new Date();
	var dateString = myDate.getFullYear() + '-' + ('0' + (myDate.getMonth()+1)).slice(-2) + '-' + ('0' + myDate.getDate()).slice(-2);

	xhttp.open("GET", diaryEndpointStubMyFitnessPal + dateString, true);
	xhttp.setRequestHeader('Authorization', 'Bearer ' + token.access_token);
	xhttp.setRequestHeader('mfp-user-id', token.user_id);
	xhttp.setRequestHeader('mfp-client-id', 'mfp-main-js')
	xhttp.send();
}

function getDiaryForTodayCronometer(sessionKey, userId){
	log('getDiaryForTodayCronometer()');
	log('sessionKey: ' + sessionKey);
	log('userId: ' + userId);
	setStatusText('Updating...');

	// load the diary today
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4) {
			log(xhttp);

			var response = JSON.parse(xhttp.responseText);
			log(response);

			hideStatusText();
			updateNetCarbDisplay(response.summary.macros.net_carbs);
		}
	};

	// diaryEndpointStub is lacking the date part, that should be tacked on to the end. ex: 2018-11-09
	var myDate = new Date();
	var dateString = myDate.getFullYear() + '-' + ('0' + (myDate.getMonth()+1)).slice(-2) + '-' + ('0' + myDate.getDate()).slice(-2);	

	var jsonData = {
		"auth": {
		   "os": "Android",
		   "api": 2,
		   "build": 268,
		   "token": sessionKey,
		   "userId": userId
		},
		"day": dateString
	}
	log('jsonData: ' + JSON.stringify(jsonData));	

	xhttp.open("POST", diaryEndpointCronometer, true);
	xhttp.setRequestHeader('User-Agent', 'okhttp/3.10.0');
	xhttp.send(JSON.stringify(jsonData));
}

function processDiaryResponseMyFitnessPal(diary){
	log('processDiaryResponseMyFitnessPal()');
	/*
	{
		"items":[
			{
				"...":"....",
				"food":{
					"...":"...",
					"nutritional_contents":{
						"...":"...",
						"fat":"...",
						"fiber":"...",
						"sugar":"..."
					}
				}
			}
		]
	}
	*/

	var totalCarbs = 0;
	var totalFiber = 0;

	log('diary items: ' + diary.items.length);
	log('-----------------------');
	for(var i = 0; i < diary.items.length; i++){
		var currentItem = diary.items[i];
		log('item servings: ' + currentItem.servings);
		log('item carbs: ' + currentItem.food.nutritional_contents.carbohydrates);
		log('item fiber: ' + currentItem.food.nutritional_contents.fiber);
		log('item carbs * servings: ' + currentItem.food.nutritional_contents.carbohydrates * currentItem.servings * currentItem.serving_size.nutrition_multiplier);
		log('item fiber * servings: ' + currentItem.food.nutritional_contents.fiber * currentItem.servings * currentItem.serving_size.nutrition_multiplier);

		totalCarbs = totalCarbs + (currentItem.food.nutritional_contents.carbohydrates * currentItem.servings * currentItem.serving_size.nutrition_multiplier);
		totalFiber = totalFiber + (currentItem.food.nutritional_contents.fiber * currentItem.servings * currentItem.serving_size.nutrition_multiplier);
		
		log('totalCarbs: ' + totalCarbs);
		log('totalFiber: ' + totalFiber);

		log('-----------------------');
	}

	var netCarbs = totalCarbs - totalFiber;
	log('net carbs: ' + netCarbs);
	updateNetCarbDisplay(netCarbs);
}

function updateNetCarbDisplay(netCarbs){
	log('updateNetCarbDisplayMyFitnessPal()');

	var roundedNetCarbs = (Math.round(netCarbs * 10) / 10).toString();

	setStatusText('Net carbs: ' + roundedNetCarbs);
	showStatusText();

	browser.browserAction.setBadgeBackgroundColor({color: 'rgb(51, 153, 51)'});
	browser.browserAction.setBadgeText({text: roundedNetCarbs});
}

function isLoggedIn(){
	log('isLoggedIn()');
	return storage.local.get('isLoggedIn');
}

function showStatusText(){
	log('showStatusText()');
	document.getElementById('status-text').classList.remove('hidden');	
}

function hideStatusText(){
	log('hideStatusText()');
	document.getElementById('status-text').classList.add('hidden');	
}

function showNetCarbs(){
	log('showNetCarbs()');
	document.getElementById('netcarbs-form').classList.remove('hidden');	
}

function hideNetCarbs(){
	log('hideNetCarbs()');
	document.getElementById('netcarbs-form').classList.add('hidden');	
}

function setStatusText(message){
	log('setStatusText()');
	document.getElementById('status-text').innerHTML = message;
}

function hideStatusTextAfterSeconds(seconds){
	log('hideStatusTextAfterSeconds()');
	setTimeout(function(){
		hideStatusText();
	}, seconds * 1000);
}

function setUserAsLoggedIn(){
	log('setUserAsLoggedIn()');

	browser.storage.local.get('userInfo').then(
		function(data){
			var userInfo = {};

			if (data && data.userInfo){
				userInfo = data.userInfo;
				userInfo.isLoggedIn = true;
			}
			else {
				log('we should not be here!');
				userInfo = {
					isLoggedIn: true
				};
			}
			saveUserInfo(userInfo);
		},
		function(error){
			log('error');
		}
	)
}

function setUserAsNotLoggedIn(){
	log('setUserAsNotLoggedIn()');

	browser.storage.local.get('userInfo').then(
		function(data){
			var userInfo = {};

			if (data && data.userInfo){
				userInfo = data.userInfo;
				userInfo.isLoggedIn = false;
			}
			else {
				userInfo = {
					isLoggedIn: false
				};
			}
			saveUserInfo(userInfo);
		},
		function(error){
			log('error');
		}
	)
}

function saveUserInfo(userInfo){
	log('saveUserInfo()');

	browser.storage.local.set({userInfo});
}

function showUpdateButton(){
	log('saveUserInfo()');

	document.getElementById('submit-form').classList.remove('hidden');	
}

function hideUpdateButton(){
	log('hideUpdateButton()');

	document.getElementById('submit-form').classList.add('hidden');	
}

function showLogoutButton(){
	log('showLogoutButton()');

	document.getElementById('clear-button').classList.remove('hidden');	
}

function hideLogoutButton(){
	log('hideLogoutButton()');

	document.getElementById('clear-button').classList.add('hidden');	
}

function showLoginForm(){
	log('showLoginForm()');
	document.getElementById('login-form').classList.remove('hidden');
}

function hideLoginForm(){
	log('hideLoginForm()');
	document.getElementById('login-form').classList.add('hidden');
}

function log(message){
	if (enableLogging){
		console.log(message);
	}
}

