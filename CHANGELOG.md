# Changelog

## 2018-11-29
* Added support for Google Chrome (v-2.0.0.0)

## 2018-11-28
* Updated `logout` function to persist local data per user preferences (v-1.1.0.1)
* Added [mfp-keto-userscript](https://github.com/Surye/mfp-keto-userscript) from Reddit user [/u/Surye](https://www.reddit.com/user/Surye) to load when viewing MFP diary (v-1.1.0.2)

## 2018-11-27
* Added support from Cronometer