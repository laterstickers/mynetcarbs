# My Net Carbs

This extension adds a button to your toolbar which lets you login to MyFitnessPal or Cronometer and calculate your net carbs for the current day.

This extension makes use of the [mfp-keto-userscript](https://github.com/Surye/mfp-keto-userscript) script by Reddit user [/u/Surye](https://www.reddit.com/user/Surye). When visiting your MFP diary, this script will automatically be injected into the page and display the additional net carbs column, as well as pie charts and addition data. For more information, view the GitHub page for the script over [here](https://github.com/Surye/mfp-keto-userscript).

## __mfp-keto-userscript FAQ__ 
(shamelessly copied from https://github.com/Surye/mfp-keto-userscript):
+ Q: Food item shows up in red and says bad data?! What gives?!
+ A: Someone added the food item incorrectly, and put net carbs in the carbs value, and so the fiber subtracts from it, making negative carbs. Nothing I can really do about this without modifying more data than I wish to.

+ Q: I get NaN's in the Net Carb column!
+ A: It may be you didn't have all the required columns added, go to http://www.myfitnesspal.com/account/diary_settings and make sure all the columns required are there (most likely, you need to add the fiber column).

+ Q: What about sugar alcohols?
+ A: Since MFP does not recognize these as a seperate nutrition, it will simply be up to the data entry (don't include it in carbs) or the user will have to know to ignore it.

## IMPORTANT NOTE: 
This extension requires your MyFitnessPal or Cronometer username and password. This extension stores your selected service (MyFitnessPal/Cronometer), username, and pending your preference, password. This extension does not record any user data for any purpose. This requires an active MyFitnessPal or Cronometer account, and entries for the current day. The author of this extension is not affiliated in any way with MyFitnessPal, Under Armor, or Cronometer.